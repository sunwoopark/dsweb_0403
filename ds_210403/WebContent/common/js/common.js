$(function(){
   
    $( "#datepicker" ).datepicker({
        showOn:"button",
        buttonImage:"/images/ico-calen.png",
        buttonImageOnly:true,
        dateFormat: 'yy-mm-dd',
    });

    $( "#datepicker-start" ).datepicker({
        showOn:"button",
        buttonImage:"/images/ico-calen.png",
        buttonImageOnly:true,
        dateFormat: 'yy-mm-dd',
    });
    
    $( "#datepicker-end" ).datepicker({
        showOn:"button",
        buttonImage:"/images/ico-calen.png",
        buttonImageOnly:true,
        dateFormat: 'yy-mm-dd',
    });

    $('.layer-pop .alert-pop .alert-close').on('click',function(){
        $(this).parent().parent().parent().hide();
    });

    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

});