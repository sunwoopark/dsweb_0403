$(function(){
   
    $( "#datepicker" ).datepicker({
        showOn:"button",
        buttonImage:"../images/ico-calen.png",
        buttonImageOnly:true,
        dateFormat: 'yy-mm-dd',
    });

    $( "#datepicker-start" ).datepicker({
        showOn:"button",
        buttonImage:"../images/ico-calen.png",
        buttonImageOnly:true,
        dateFormat: 'yy-mm-dd',
    });
    
    $( "#datepicker-end" ).datepicker({
        showOn:"button",
        buttonImage:"../images/ico-calen.png",
        buttonImageOnly:true,
        dateFormat: 'yy-mm-dd',
    });

    var $layerDim = $('.layer-pop');
    
    $(document).on('click',function(e){
		if($layerDim.is(e.target)){
			$layerDim.hide();
		}
    });    
    
    $('.layer-pop .alert-pop .alert-close').on('click',function(){
		$layerDim.hide();
    });
    

    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

    $('.adm-tab ul>li>a').on('click',function(){
        $(this).parent().parent().find('li').removeClass('on')
        $(this).parent().addClass('on')
    });


});