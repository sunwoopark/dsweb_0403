package ds.visual.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import ds.common.common.CommandMap;
import ds.common.dao.AbstractDAO;

@Repository("visualDAO")
public class VisualDAO extends AbstractDAO {

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAccident(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("accident.accident", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAccident2(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("accident.accident2", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAccidentYear(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("accident.accidentyear", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectDisabled(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("disabled.cctv", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFine(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("disabled.fine", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectParking(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("disabled.parking", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTraffic(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("complaintt.comtra", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTraffic2(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("complaintt.comtra2", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTrafficYear(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("complaintt.comtrayear", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectComParking(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("complaintp.compark", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectComParking2(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("complaintp.compark2", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectComParkingYear(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("complaintp.comparkyear", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectHangfine(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("hang_fine.hang_fine", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectWorkchild(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("workcctv.workchild", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectWorkop(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("workcctv.workop", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCctvtraffic(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("workcctv.cctvtr", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCctvcp(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("workcctv.cctvcp", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFireplug(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("fireplug.fireplug", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFireplugYear(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("fireplug.year", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectNeglect(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("neglect.neglect", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectNeglectYear(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("neglect.year", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectNightpark(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("nightpark.nightpark", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectNightpark1(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("nightpark.nightpark1", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectNightpark2(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("nightpark.nightpark2", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectNightparkYear(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("nightpark.year", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTransit(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("transit.transit", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTransit1(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("transit.transit1", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTransit2(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("transit.transit2", map);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTransitDong(Map<String, Object> map) throws Exception {
		return (List<Map<String, Object>>) selectList("transit.dong", map);
	}

	// 210225_신규내용 추가
	// 1. 교통민원
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectComplaintTraffic(String std_yy) throws Exception {
		return (List<Map<String, Object>>) selectList("complaintt.selectComplaintTraffic", std_yy);
	}

	// 2. 주차민원
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectComplaintParking(String std_yy) {
		return (List<Map<String, Object>>) selectList("complaintp.selectComplaintParking", std_yy);
	}

	// 3-1 월별교통사고발생건수
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAccidentTrafficMM(String std_yy) {
		return (List<Map<String, Object>>) selectList("accident.selectAccidentTrafficMM", std_yy);
	}

	// 3-2 요일별교통사고발생건수
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAccidentTrafficDay(String std_yy) {
		return (List<Map<String, Object>>) selectList("accident.selectAccidentTrafficDay", std_yy);
	}

	// 4-1 행정동별 소화전 개수
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFirePlugInfo(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("fireplug.selectFirePlugInfo", map);
	}

	// 4-3 행정동별 불법주차 단속 건수
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIllegalParkingYY(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("fireplug.selectIllegalParkingYY", map);
	}

	// 4-2 연도별월별 불법주차 단속 현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIllegalParkingState(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("fireplug.selectIllegalParkingState", map);
	}

	// 5-2 행정동별 일평균 유동인구수 및 대상건물수 현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIllegalParkingAvg(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("hang_fine.selectIllegalParkingAvg", map);
	}

	// 7-1 행정동별불법주정차CCTV현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCctvCntDo(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("disabled.selectCctvCntDo", map);
	}

	// 7-2 월별장애인주차구역불법주차단속현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectDisabledParkingMM(String std_yy) {
		return (List<Map<String, Object>>) selectList("disabled.selectDisabledParkingMM", std_yy);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectDisabledParkingMM2(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("disabled.selectDisabledParkingMM2", map);
	}

	// 7-3 장애인주차구역불법주차 행정동별 단속현황
	@SuppressWarnings("unchecked")

	public List<Map<String, Object>> selectDisabledParkingState(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("disabled.selectDisabledParkingState", map);
	}

	// 7-4 행정동별장애인주차장현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectDisabledParkingDo(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("disabled.selectDisabledParkingDo", map);
	}

	// 8-1 행정동별 어린이 노인 시설 현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectWalkBuilDo(String buil_type) {
		return (List<Map<String, Object>>) selectList("workcctv.selectWalkBuilDo", buil_type);
	}

	// 8-2육교 설치현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectOverpassState(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("workcctv.selectOverpassState", map);
	}

	// 8-3 교통CCTV 설치 현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTrafficCctvState(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("workcctv.selectTrafficCctvState", map);
	}

	// 8-4 방범CCTV 설치 현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCrimeCctvState(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("workcctv.selectCrimeCctvState", map);
	}

	// 9-1월별화물차밤샘주차단속현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIllegalParkingMM(String std_yy) {
		return (List<Map<String, Object>>) selectList("nightpark.selectIllegalParkingMM", std_yy);
	}

	// 9-2 행정동별 불법주차 단속건수
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIllegalParkingDo(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("nightpark.selectIllegalParkingDo", map);
	}

	// 10-1행정동별화물차운송사업자현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCargoState(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("transit.selectCargoState", map);
	}

	// 10-2화물차운송유형별비중
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCargoType(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("transit.selectCargoType", map);
	}

	// 6-1 연도별월별무단방치차량단속현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCarAccidentYY(String std_yy) {
		return (List<Map<String, Object>>) selectList("neglect.selectCarAccidentYY", std_yy);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCarAccidentYY2(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("neglect.selectCarAccidentYY2", map);
	}

	// 6-2행정동별무단방치차량현황
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCarAccidentDo(String car_type) {
		return (List<Map<String, Object>>) selectList("neglect.selectCarAccidentDo", car_type);
	}


}
