package ds.visual.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import ds.visual.dao.VisualDAO;

@Service("visualService")
public class VisualServiceImpl implements VisualService {
	@Resource(name = "visualDAO")
	private VisualDAO visualDAO;

	@Override
	public List<Map<String, Object>> selectAccident(Map<String, Object> map) throws Exception {
		return visualDAO.selectAccident(map);
	}

	@Override
	public List<Map<String, Object>> selectAccident2(Map<String, Object> map) throws Exception {
		return visualDAO.selectAccident2(map);
	}

	@Override
	public List<Map<String, Object>> selectAccidentYear(Map<String, Object> map) throws Exception {
		return visualDAO.selectAccidentYear(map);
	}

	@Override
	public List<Map<String, Object>> selectDisabled(Map<String, Object> map) throws Exception {
		return visualDAO.selectDisabled(map);
	}

	@Override
	public List<Map<String, Object>> selectFine(Map<String, Object> map) throws Exception {
		return visualDAO.selectFine(map);
	}

	@Override
	public List<Map<String, Object>> selectParking(Map<String, Object> map) throws Exception {
		return visualDAO.selectParking(map);
	}

	@Override
	public List<Map<String, Object>> selectTraffic(Map<String, Object> map) throws Exception {
		return visualDAO.selectTraffic(map);
	}

	@Override
	public List<Map<String, Object>> selectTraffic2(Map<String, Object> map) throws Exception {
		return visualDAO.selectTraffic2(map);
	}

	@Override
	public List<Map<String, Object>> selectTrafficYear(Map<String, Object> map) throws Exception {
		return visualDAO.selectTrafficYear(map);
	}

	@Override
	public List<Map<String, Object>> selectComParking(Map<String, Object> map) throws Exception {
		return visualDAO.selectComParking(map);
	}

	@Override
	public List<Map<String, Object>> selectComParking2(Map<String, Object> map) throws Exception {
		return visualDAO.selectComParking2(map);
	}

	@Override
	public List<Map<String, Object>> selectComParkingYear(Map<String, Object> map) throws Exception {
		return visualDAO.selectComParkingYear(map);
	}

	@Override
	public List<Map<String, Object>> selectHangfine(Map<String, Object> map) throws Exception {
		return visualDAO.selectHangfine(map);
	}

	@Override
	public List<Map<String, Object>> selectWorkop(Map<String, Object> map) throws Exception {
		return visualDAO.selectWorkop(map);
	}

	@Override
	public List<Map<String, Object>> selectWorkchild(Map<String, Object> map) throws Exception {
		return visualDAO.selectWorkchild(map);
	}

	@Override
	public List<Map<String, Object>> selectCctvtraffic(Map<String, Object> map) throws Exception {
		return visualDAO.selectCctvtraffic(map);
	}

	@Override
	public List<Map<String, Object>> selectCctvcp(Map<String, Object> map) throws Exception {
		return visualDAO.selectCctvcp(map);
	}

	@Override
	public List<Map<String, Object>> selectFireplug(Map<String, Object> map) throws Exception {
		return visualDAO.selectFireplug(map);
	}

	@Override
	public List<Map<String, Object>> selectFireplugYear(Map<String, Object> map) throws Exception {
		return visualDAO.selectFireplugYear(map);
	}

	@Override
	public List<Map<String, Object>> selectNeglect(Map<String, Object> map) throws Exception {
		return visualDAO.selectNeglect(map);
	}

	@Override
	public List<Map<String, Object>> selectNeglectYear(Map<String, Object> map) throws Exception {
		return visualDAO.selectNeglectYear(map);
	}

	@Override
	public List<Map<String, Object>> selectNightpark(Map<String, Object> map) throws Exception {
		return visualDAO.selectNightpark(map);
	}

	@Override
	public List<Map<String, Object>> selectNightpark1(Map<String, Object> map) throws Exception {
		return visualDAO.selectNightpark1(map);
	}

	@Override
	public List<Map<String, Object>> selectNightpark2(Map<String, Object> map) throws Exception {
		return visualDAO.selectNightpark2(map);
	}

	@Override
	public List<Map<String, Object>> selectNightparkYear(Map<String, Object> map) throws Exception {
		return visualDAO.selectNightparkYear(map);
	}

	@Override
	public List<Map<String, Object>> selectTransit(Map<String, Object> map) throws Exception {
		return visualDAO.selectTransit(map);
	}

	@Override
	public List<Map<String, Object>> selectTransit1(Map<String, Object> map) throws Exception {
		return visualDAO.selectTransit1(map);
	}

	@Override
	public List<Map<String, Object>> selectTransit2(Map<String, Object> map) throws Exception {
		return visualDAO.selectTransit2(map);
	}

	@Override
	public List<Map<String, Object>> selectTransitDong(Map<String, Object> map) throws Exception {
		return visualDAO.selectTransitDong(map);
	}

	// 210225_신규내용 추가
	// 1. 교통민원
	@Override
	public List<Map<String, Object>> selectComplaintTraffic(String std_yy) throws Exception {
		return visualDAO.selectComplaintTraffic(std_yy);
	}

	// 2. 주차민원
	@Override
	public List<Map<String, Object>> selectComplaintParking(String std_yy) throws Exception {
		return visualDAO.selectComplaintParking(std_yy);
	}

	// 3-1월별교통사고발생건수
	@Override
	public List<Map<String, Object>> selectAccidentTrafficMM(String std_yy) throws Exception {
		return visualDAO.selectAccidentTrafficMM(std_yy);
	}

	// 3-2 요일별교통사고발생건수
	@Override
	public List<Map<String, Object>> selectAccidentTrafficDay(String std_yy) throws Exception {
		return visualDAO.selectAccidentTrafficDay(std_yy);
	}

	// 4-1 행정동별 소화전 개수
	@Override
	public List<Map<String, Object>> selectFirePlugInfo(Map<String, Object> map) throws Exception {
		return visualDAO.selectFirePlugInfo(map);
	}

	// 4-3 행정동별 불법주차 단속 건수
	@Override
	public List<Map<String, Object>> selectIllegalParkingYY(Map<String, Object> map) throws Exception {
		return visualDAO.selectIllegalParkingYY(map);
	}

	// 4-2 연도별월별 불법주차 단속 현황
	@Override
	public List<Map<String, Object>> selectIllegalParkingState(Map<String, Object> map) {
		return visualDAO.selectIllegalParkingState(map);
	}

	// 5-2 행정동별 일평균 유동인구수 및 대상건물수 현황
	@Override
	public List<Map<String, Object>> selectIllegalParkingAvg(Map<String, Object> map) throws Exception {
		return visualDAO.selectIllegalParkingAvg(map);
	}

	// 7-1 행정동별불법주정차CCTV현황
	@Override
	public List<Map<String, Object>> selectCctvCntDo(Map<String, Object> map) {
		return visualDAO.selectCctvCntDo(map);
	}

	// 7-2 월별장애인주차구역불법주차단속현황
	@Override
	public List<Map<String, Object>> selectDisabledParkingMM(String std_yy) {
		return visualDAO.selectDisabledParkingMM(std_yy);
	}
	
	@Override
	public List<Map<String, Object>> selectDisabledParkingMM2(Map<String, Object> map) {
		return visualDAO.selectDisabledParkingMM2(map);
	}

	// 7-3 장애인주차구역불법주차 행정동별 단속현황
	@Override
	public List<Map<String, Object>> selectDisabledParkingState(Map<String, Object> map) {
		return visualDAO.selectDisabledParkingState(map);
	}

	// 7-4 행정동별장애인주차장현황
	@Override
	public List<Map<String, Object>> selectDisabledParkingDo(Map<String, Object> map) {
		return visualDAO.selectDisabledParkingDo(map);
	}

	// 8-1 행정동별 어린이 노인 시설 현황
	@Override
	public List<Map<String, Object>> selectWalkBuilDo(String buil_type) throws Exception {
		return visualDAO.selectWalkBuilDo(buil_type);
	}

	// 8-2육교 설치현황
	@Override
	public List<Map<String, Object>> selectOverpassState(Map<String, Object> map) {
		return visualDAO.selectOverpassState(map);
	}

	// 8-3 교통CCTV 설치 현황
	@Override
	public List<Map<String, Object>> selectTrafficCctvState(Map<String, Object> map) throws Exception {
		return visualDAO.selectTrafficCctvState(map);
	}

	// 8-4 방범CCTV 설치 현황
	@Override
	public List<Map<String, Object>> selectCrimeCctvState(Map<String, Object> map) throws Exception {
		return visualDAO.selectCrimeCctvState(map);
	}

	// 9-1월별화물차밤샘주차단속현황
	@Override
	public List<Map<String, Object>> selectIllegalParkingMM(String std_yy) throws Exception {
		return visualDAO.selectIllegalParkingMM(std_yy);
	}

	// 9-2 행정동별 불법주차 단속건수
	@Override
	public List<Map<String, Object>> selectIllegalParkingDo(Map<String, Object> map) throws Exception {
		return visualDAO.selectIllegalParkingDo(map);
	}

	// 10-1행정동별화물차운송사업자현황
	@Override
	public List<Map<String, Object>> selectCargoState(Map<String, Object> map) throws Exception {
		return visualDAO.selectCargoState(map);

	}
	// 10-2화물차운송유형별비중
	@Override
	public List<Map<String, Object>> selectCargoType(Map<String, Object> map) throws Exception {
		return visualDAO.selectCargoType(map);
	}
	
	// 6-1 연도별월별무단방치차량단속현황 
	@Override
	public List<Map<String, Object>> selectCarAccidentYY(String std_yy) throws Exception {
		return visualDAO.selectCarAccidentYY(std_yy);
	}
	@Override
	public List<Map<String, Object>> selectCarAccidentYY2(Map<String, Object> map) throws Exception {
		return visualDAO.selectCarAccidentYY2(map);
	}
	

	// 6-2행정동별무단방치차량현황
	@Override
	public List<Map<String, Object>> selectCarAccidentDo(String car_type) throws Exception {
		return visualDAO.selectCarAccidentDo(car_type);
	}
	
}
