package ds.common.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ds.common.common.CommandMap;
import ds.common.service.CommonService;

@Controller
public class CommonController {
    Logger log = Logger.getLogger(this.getClass());
     
    @Resource(name="commonService")
    private CommonService commonService;
    
    /**
     * Use Log Insert
     * @param commandMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/proc-write-userlog.do")
	public ModelAndView insertUserLog(CommandMap commandMap, HttpServletRequest request) throws Exception {
    	ModelAndView mv = new ModelAndView("jsonView");
    	
    	HttpSession session = request.getSession();
    	String sSessionId = session.getId().toString() ;
    	String sUserId =  (String) session.getAttribute("sessionKeyUserId");
    	
    	commandMap.put("session_id",sSessionId) ;
    	commandMap.put("user_id",sUserId) ;
		
    	int iRtn = (int) commonService.insertUseLog(commandMap.getMap());
    	
    	mv.addObject("result", iRtn);

        return mv;
	}
    
    /**
     * Menu 권한 Select
     * @param commandMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/select-menu-arth.do")
	public ModelAndView selectMenuArth(CommandMap commandMap) throws Exception {
    	ModelAndView mv = new ModelAndView("jsonView");
    	
    	Map<String, Object> map = commonService.selectMenuArth(commandMap.getMap());
		mv.addObject("map", map);

        return mv;
	}

}