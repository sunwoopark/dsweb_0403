package ds.common.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository("commonDAO")
public class CommonDAO extends AbstractDAO{

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectCmnCdList(String majorCd) throws Exception{
		return (List<Map<String, Object>>)selectList("common.selectCmnCdlList", majorCd) ;
	}

	public Object insertUseLog(Map<String, Object> map) {
		return insert("common.insertUseLog", map); 
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> selectMenuArth(Map<String, Object> map) throws Exception{
		return (Map<String, Object>) selectOne("common.selectMenuArth", map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> selectCmnCd(Map<String, Object> map) {
		return (Map<String, Object>) selectOne("common.selectCmnCd", map);
	}
	
}