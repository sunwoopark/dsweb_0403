package ds.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    
    /**
     * 배열을 구분자로 구분하여 문자열 변환
     * 
     * @param o
     *            : 배열
     * @param division
     *            : 구분자
     * @return
     */
    public static String arrayToString(String[] o, String division) {
        if (o.length == 0)
            return "";

        String str = "";
        for (int i = 0; i < o.length; i++) {
            str += o[i] + division;
        }

        return str.substring(0, str.length() - 1);
    }

    /**
     * 공백이거나 널인 경우 true 반환
     * 
     * @param o
     *            검사할 객체
     * @return 공백이나 널인경우 true 아닌 경우는 false
     */
    public static boolean isEmpty(Object o) {
        if (o == null)
            return true;
        else if ("".equals(o))
            return true;
        else
            return false;
    }

    /**
     * 비교할 문자열이 배열로 넘어온 문자열들에 포함되었는지 검사
     * 
     * @param arr
     *            문자열배열
     * @param compare
     *            검사할 문자열
     * @return
     */
    public static boolean arrayEquals(String[] arr, String compare) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(compare))
                return true;
        }

        return false;
    }

    /**
     * 문자열안에 검사할 문자열이 몇개인지 반환
     * 
     * <pre>
     * 예제)
     * int Cnt = fxWordCount(&quot;aaabbbcccaaa&quot;, &quot;aaa&quot;);
     * Cnt는 2
     * </pre>
     * 
     * @param s
     *            전체 문자열
     * @param w
     *            검사할 문자열
     * @return 검사된 개수
     */
    public static int wordCount(String s, String w) {
        int cnt = 0;

        do {
            if (s.indexOf(w) > -1) {
                cnt++;
                s = s.substring(s.indexOf(w) + w.length());
            }
        } while (s.indexOf(w) > -1);

        return cnt;
    }

    /**
     * 주어진 길이보다 부족한 길이만큼 지정한 문자 앞에 붙치기
     * 
     * <pre>
     * 예제)
     * String Rep = fxReplicate(10, &quot;aaaaa&quot;, &quot;b&quot;);
     * Rep는 &quot;bbbbbaaaaa&quot;
     * </pre>
     * 
     * @param l
     *            문자열 길이
     * @param s
     *            문자열
     * @param c
     *            추가할 문자
     * @return 문자열
     */
    public static String replicate(int l, String s, String c) {
        for (int i = s.length(); i < l; i++) {
            s = c + s;
        }

        return s;
    }

    /**
     * 주어진 길이보다 부족한 길이만큼 지정한 문자 뒤에 붙치기
     * 
     * <pre>
     * 예제)
     * String Rep = fxReplicateRev(10, &quot;aaaaa&quot;, &quot;b&quot;);
     * Rep는 &quot;aaaaabbbbb&quot;
     * </pre>
     * 
     * @param l
     *            문자열 길이
     * @param s
     *            문자열
     * @param c
     *            추가할 문자
     * @return 문자열
     */
    public static String replicateRev(int l, String s, String c) {
        for (int i = s.length(); i < l; i++) {
            s = s + c;
        }

        return s;
    }

    /**
     * html 태그 제거
     * 
     * @param s
     *            태그제거할 문자열
     * @return 태그제거한 문자열
     */
    public static String deleteTag(String s) {
        if (!isEmpty(s)) {
            Pattern p = Pattern.compile("\\<(\\/?)(\\w+)*([^<>]*)>");
            Matcher m = p.matcher(s);
            s = m.replaceAll("");
        }

        return s;
    }

    /**
     * XSS 관련 문자를 아스키코드값으로 변환
     * 
     * @param s
     *            변환할 문자열
     * @return 변환한 문자열
     */
    public static String convertCharToAscii(String s) {
        if (!isEmpty(s)) {
            s = s.replaceAll("&", "&amp;");
            s = s.replaceAll("<", "&lt;");
            s = s.replaceAll(">", "&gt;");
            s = s.replaceAll("'", "&#39;");
            s = s.replaceAll("\"", "&#34;");
        }

        return s;
    }

    /**
     * 아스키코드값을 문자로 변환
     * 
     * @param s
     *            변환할 문자열
     * @return 변환한 문자열
     */
    public static String convertAsciiToChar(String s) {
        if (!isEmpty(s)) {
            s = s.replaceAll("&lt;", "<");
            s = s.replaceAll("&gt;", ">");
            s = s.replaceAll("&#39;", "'");
            s = s.replaceAll("&#34;", "\"");
            s = s.replaceAll("&amp;", "&");
        }

        return s;
    }
    
    /**
	 * 입력받은 문자열이 null 이거나 NULL 이거나 "" 이면 원하는 문자열로 변환
	 * 
	 * @param value 입력받은 문자열
	 * @param defaultValue 변환할 문자열
	 * @return 변환처리된 문자열
	 */
	public static String getValue(String value, String defaultValue) {
		if(value == null) {
			return defaultValue;
		} else if(value.equals("null")) {
			return defaultValue;
		} else if(value.equals("NULL")) {
			return defaultValue;
		} else if(value.equals("")) {
			return defaultValue;
		} else {
			return value;
		}
	}
    
    /**
	 * select 형태로 시간을 반환 (00~23)
	 * 
	 * @param hour 시간명
	 * @param rHour 시간
	 * @param topValue 상단 값
	 * @param topValueName 상단 명
	 * @param etc 기타 스크립트 코드 입력
	 * @return select 형태
	 * @throws Exception
	 */
	public static String getSelectHour(String hour, String rHour, String topValue, String topValueName, String etc) throws Exception {
		StringBuffer view = new StringBuffer();
		
		String hourStr = "";
		
		try {
			if(rHour.length() < 2) {
				rHour = "0" + rHour;
			}
			view.append("<select id = " + hour + " name=" + hour + " " + etc + " >   \n");
			if(!StringUtil.getValue(topValue, "").equals("")) {
				view.append("     <option value='" + topValue + "' ");
				if(rHour != null && rHour.equals(topValue)) {
					view.append ("selected ");
				}
				view.append(">" + topValueName + "</option>   \n");
			}
			for(int kk=0; kk<=23; kk++) {
				if(kk < 10) {
					hourStr = "0" + kk + "";
				} else {
					hourStr = kk + "";
				}
				view.append("     <option value='" + hourStr + "' ");
				if(rHour != null && rHour.equals(hourStr)) {
					view.append("selected ");
				}
				view.append(">" + hourStr + "</option>   \n");
			}
			view.append("</select>   \n");
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception(e.toString());
		} finally {
			
		}
		
		return view.toString();
	}
	
	/**
	 * select 형태로 시간을 반환 (01~24)
	 * 
	 * @param hour 시간명
	 * @param rHour 시간
	 * @param topValue 상단 값
	 * @param topValueName 상단 명
	 * @param etc 기타 스크립트 코드 입력
	 * @return select 형태
	 * @throws Exception
	 */
	public static String getSelectHour1(String hour, String rHour, String topValue, String topValueName, String etc) throws Exception {
		StringBuffer view = new StringBuffer();
		
		String hourStr = "";
		
		try {
			if(rHour.length() < 2) {
				rHour = "0" + rHour;
			}
			view.append("<select id = " + hour + " name=" + hour + " " + etc + " >   \n");
//			view.append("<select name=" + hour + " " + etc + " >   \n");
			if(!StringUtil.getValue(topValue, "").equals("")) {
				view.append("     <option value='" + topValue + "' ");
				if(rHour != null && rHour.equals(topValue)) {
					view.append("selected ");
				}
				view.append(">" + topValueName + "</option>   \n");
			}
			for(int kk=1; kk<=24; kk++) {
				if(kk < 10) {
					hourStr = "0" + kk + "";
				} else {
					hourStr = kk + "";
				}
				view.append("     <option value='" + hourStr + "' ");
				if(rHour != null && rHour.equals(hourStr)) {
					view.append("selected ");
				}
				view.append(">" + hourStr + "</option>   \n");
			}
			view.append("</select>   \n");
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception(e.toString());
		} finally {
			
		}
		
		return view.toString();
	}
	
	/**
	 * select 형태로 분을 반환
	 * 
	 * @param min 분 명
	 * @param rMin 분 값
	 * @param topValue 상단 값
	 * @param topValueName 상단 명
	 * @param etc 기타 스크립트 코드 입력
	 * @return select 형태
	 * @throws Exception
	 */
	public static String getSelectMin(String min, String rMin, String topValue, String topValueName, String etc) throws Exception {
StringBuffer view = new StringBuffer();
		
		String minStr = "";
		
		try {
			view.append("<select name=" + min + " " + etc + " >   \n");
			if(!StringUtil.getValue(topValue, "").equals("")) {
				view.append("     <option value='" + topValue + "' ");
				if(rMin != null && rMin.equals(topValue)) {
					view.append("selected ");
				}
				view.append(">" + topValueName + "</option>   \n");
			}
			for(int kk=0; kk<=11; kk++) {
				minStr = Integer.toString(5*kk);
				if(minStr.length() < 2) {
					minStr = "0" + minStr;
				}
				view.append("<option value='" + minStr + "' ");
				if(rMin != null && rMin.equals(minStr)) {
					view.append("selected ");
				}
				view.append(">" + minStr + "</option>   \n");
			}
			view.append("</select>   \n");
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception(e.toString());
		} finally {
			
		}
		
		return view.toString();
	}
	
	/**
	 * select 형태로 분을 반환
	 * 
	 * @param min 분명
	 * @param rMin 분값
	 * @param topValue 상단 값
	 * @param topValueName 상단 명
	 * @param etc 기타스크립트 코드 입력
	 * @return select 형태
	 * @throws Exception
	 * 
	 */
	public static String getSelectMin60(String min, String rMin, String topValue, String topValueName, String etc) throws Exception {
		StringBuffer view = new StringBuffer();
		
		String minStr = "";
		
		try {
			if(rMin.length() < 2) {
				rMin = "0" + rMin;
			}
			view.append("<select name=" + min + " " + etc + " >   \n");
			if(!StringUtil.getValue(topValue, "").equals("")) {
				view.append("     <option value='" + topValue + "' ");
				if(rMin != null && rMin.equals(topValue)) {
					view.append("selected ");
				}
				view.append(">" + topValueName + "</option>   \n");
			}
			for(int kk=0; kk<60; kk++) {
				if(kk < 10) {
					minStr = "0" + kk;
				} else {
					minStr = kk + "";
				}
				view.append("     <option value='" + minStr + "' ");
				if(rMin != null && rMin.equals(minStr)) {
					view.append("selected ");
				}
				view.append(">" + minStr + "</option>   \n");
			}
			view.append("</select>   \n");
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception(e.toString());
		} finally {
			
		}
		
		return view.toString();
	}
	
	/**
	 * select 형태로 시간을 반환 (00~23)
	 * 
	 * @param hour 시간명
	 * @param rHour 시간
	 * @param topValue 상단 값
	 * @param topValueName 상단 명
	 * @param etc 기타 스크립트 코드 입력
	 * @return select 형태
	 * @throws Exception
	 */
	public static String getSelectSearchHour(String hour, String rHour, String etc) throws Exception {
		StringBuffer view = new StringBuffer();
		
		String hourStr = "";
		
		try {
			if(rHour.length() < 2) {
				rHour = "0" + rHour;
			}
			view.append("<select name=" + hour + " " + etc + " >   \n");
			
			view.append("     <option value=''>전체</option>   \n");
			for(int kk=0; kk<=23; kk++) {
				if(kk < 10) {
					hourStr = "0" + kk + "";
				} else {
					hourStr = kk + "";
				}
				view.append("     <option value='" + hourStr + "' ");
				if(rHour != null && rHour.equals(hourStr)) {
					view.append("selected ");
				}
				view.append(">" + hourStr + "</option>   \n");
			}
			view.append("</select>   \n");
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception(e.toString());
		} finally {
			
		}
		
		return view.toString();
	}
	
	/**
	 * select 형태로 분을 반환
	 * 
	 * @param min 분명
	 * @param rMin 분값
	 * @param topValue 상단 값
	 * @param topValueName 상단 명
	 * @param etc 기타스크립트 코드 입력
	 * @return select 형태
	 * @throws Exception
	 * 
	 */
	public static String getSelectMin60Unit5(String min, String rMin, String topValue, String topValueName, String etc) throws Exception {
		StringBuffer view = new StringBuffer();

		String minStr = "";

		try {
			if(rMin.length() < 2) {
				rMin = "0" + rMin;
			}
			view.append("<select id=" + min + " name=" + min + " " + etc + " >   \n");
//			view.append("<select name=" + min + " " + etc + " >   \n");
			if(!StringUtil.getValue(topValue, "").equals("")) {
				view.append("     <option value='" + topValue + "' ");
				if(rMin != null && rMin.equals(topValue)) {
					view.append("selected ");
				}
				view.append(">" + topValueName + "</option>   \n");
			}
			for(int kk = 0; kk < 60; kk++) {
				if(kk < 10) {
					minStr = "0" + kk;
				} else {
					minStr = kk + "";
				}
				if(kk % 5==0){
					view.append("     <option value='" + minStr + "' ");
					if(rMin != null && rMin.equals(minStr)) {
						view.append("selected ");
					}
					view.append(">" + minStr + "</option>   \n");
				}
			}
			view.append("</select>   \n");
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception(e.toString());
		} finally {

		}

		return view.toString();
	}
    
    

    private static final Pattern SCRIPT = Pattern.compile("(?i)<(no)?script[^>]*>.*?</(no)?script[^>]*>", Pattern.DOTALL);

    private static final Pattern TITLE = Pattern.compile("(?i)<title[^>]*>[^(<title)].*?</title[^>]*>", Pattern.DOTALL);

    private static final Pattern IFRAME = Pattern.compile("(?i)<iframe[^>]*>[^(<iframe)].*?</iframe[^>]*>", Pattern.DOTALL);

    private static final Pattern STYLE = Pattern.compile("(?i)<style[^>]*>[^(<style)].*?</style[^>]*>", Pattern.DOTALL);

    private static final Pattern FRAMESET = Pattern.compile("(?i)<frameset[^>]*>[^(<frameset)].*?</frameset[^>]*>", Pattern.DOTALL);

    private static final Pattern HEAD = Pattern.compile("(?i)<head[^>]*>[^(<head)].*?</head[^>]*>", Pattern.DOTALL);

    private static final Pattern MAP = Pattern.compile("(?i)<map[^>]*>[^(<map)].*?</map[^>]*>", Pattern.DOTALL);

    private static final Pattern APPLET = Pattern.compile("(?i)<applet[^>]*>[^(<applet)].*?</applet[^>]*>", Pattern.DOTALL);

    private static final Pattern DELETETAGS = Pattern.compile("(?i)<(!doctype|iframe|title|link|style|script|frameset|meta|base|map|applet)+[^>]*>", Pattern.DOTALL);

    private static final Pattern COMMENT = Pattern.compile("/\\*.*?\\*/", Pattern.DOTALL);

    private static final Pattern HTMLCOMMENT = Pattern.compile("<!--.*?-->", Pattern.DOTALL);

    private static final Pattern XMLDATA = Pattern.compile("(?i)<!\\[CDATA\\[.*?\\]\\]>", Pattern.DOTALL);

    private static final Pattern EXPRESSION = Pattern.compile("(?i):[ ]*expression\\(");

    private static final Pattern VBSCRIPT = Pattern.compile("(?i)vbscript:");

    private static final Pattern JAVASCRIPT = Pattern.compile("j[ ]*a[ ]*v[ ]*a[ ]*s[ ]*c[ ]*r[ ]*i[ ]*p[ ]*t[ ]*:");

    private static final Pattern TAGS = Pattern.compile("(?i)<[/]*(body|html|form|input|select|option|textarea)[^>]*>", Pattern.DOTALL);

    private static final Pattern EVENTHANDLERS = Pattern.compile("(?i)<[^<^>]*(FSCommand|onAbort|onActivate|onAfterPrint|onAfterUpdate|onBeforeActivate|onBeforeCopy|onBeforeCut|onBeforeDeactivate|onBeforeEditFocus|onBeforePaste|onBeforePrint|onBeforeUnload|onBegin|onBlur|onBounce|onCellChange|onChange|onClick|onContextMenu|onControlSelect|onCopy|onCut|onDataAvailable|onDataSetChanged|onDataSetComplete|onDblClick|onDeactivate|onDrag|onDragEnd|onDragLeave|onDragEnter|onDragOver|onDragDrop|onDrop|onEnd|onError|onErrorUpdate|onFilterChange|onFinish|onFocus|onFocusIn|onFocusOut|onHelp|onKeyDown|onKeyPress|onKeyUp|onLayoutComplete|onLoad|onLoseCapture|onMediaComplete|onMediaError|onMouseDown|onMouseEnter|onMouseLeave|onMouseMove|onMouseOut|onMouseOver|onMouseUp|onMouseWheel|onMove|onMoveEnd|onMoveStart|onOutOfSync|onPaste|onPause|onProgress|onPropertyChange|onReadyStateChange|onRepeat|onReset|onResize|onResizeEnd|onResizeStart|onResume|onReverse|onRowsEnter|onRowExit|onRowDelete|onRowInserted|onScroll|onSeek|onSelect|onSelectionChange|onSelectStart|onStart|onStop|onSyncRestored|onSubmit|onTimeError|onTrackChange|onUnload|onURLFlip|seekSegmentTime)[ ]*=", Pattern.DOTALL);

    public static String convertTag(String htmlString) {
        htmlString = SCRIPT.matcher(htmlString).replaceAll("");
        htmlString = TITLE.matcher(htmlString).replaceAll("");
        htmlString = IFRAME.matcher(htmlString).replaceAll("");
        htmlString = STYLE.matcher(htmlString).replaceAll("");
        htmlString = FRAMESET.matcher(htmlString).replaceAll("");
        htmlString = HEAD.matcher(htmlString).replaceAll("");
        htmlString = MAP.matcher(htmlString).replaceAll("");
        htmlString = APPLET.matcher(htmlString).replaceAll("");
        htmlString = DELETETAGS.matcher(htmlString).replaceAll("");
        htmlString = COMMENT.matcher(htmlString).replaceAll("");
        htmlString = HTMLCOMMENT.matcher(htmlString).replaceAll("");
        htmlString = XMLDATA.matcher(htmlString).replaceAll("");
        htmlString = EXPRESSION.matcher(htmlString).replaceAll(":expression<x/>(");
        htmlString = VBSCRIPT.matcher(htmlString).replaceAll("vbscript<x/>:");

        // UTF-8 Unicode encoding, Long UTF-8 Unicode encoding, Hex encoding 된
        // 문자중 알파벳에 해당하는 문자는 특정문자로 치환한다.
        for (char c = 65; c <= 122; c++) {
            String hexNumUpperCase = Long.toHexString(Long.parseLong("" + (int) c)).toUpperCase();
            String hexNumLowerCase = Long.toHexString(Long.parseLong("" + (int) c)).toLowerCase();
            // UTF-8 Unicode
            htmlString = htmlString.replaceAll("&#" + (int) c + ";", ".");
            // Long UTF-8 Unicode
            htmlString = htmlString.replaceAll("&#" + StringUtil.replicate(7, "" + (int) c, "0"), ".");
            // Hex Unicode
            htmlString = htmlString.replaceAll("&#x" + hexNumUpperCase, ".");
            htmlString = htmlString.replaceAll("&#x" + hexNumLowerCase, ".");
        }

        // convert javascript
        Matcher matcher = JAVASCRIPT.matcher(htmlString);
        StringBuffer sb = new StringBuffer();

        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group().substring(0, matcher.group().length() - 1) + "<x/>:");
        }

        matcher.appendTail(sb);

        // convert tag
        matcher = TAGS.matcher(sb.toString());
        sb = new StringBuffer();

        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group().replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
        }

        matcher.appendTail(sb);

        // delete event handler
        matcher = EVENTHANDLERS.matcher(sb.toString());
        sb = new StringBuffer();

        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group().substring(0, matcher.group().length() - 1) + "&#61;");
        }

        matcher.appendTail(sb);

        return sb.toString();
    }

    /**
     * 지정한 길이만큼만 보여주고 나머지 생략. 전체문자열은 툴팁으로 보여줌
     * 
     * @param t
     *            원 문자열
     * @param l
     *            보여줄 길이
     * @return
     */
    public static String subStringToolTip(String t, int l) {
        return "<div style=\"width:100%; overflow:hidden; text-overflow:ellipsis;\" title=\"" + convertCharToAscii(t) + "\"><nobr>" + convertCharToAscii(subString(t, 0, l, "..")) + "</nobr></div>";
    }

    /**
     * 지정한 길이만큼만 보여주고 나머지 생략.
     * 
     * @param t
     *            원 문자열
     * @param s
     *            시작위치
     * @param e
     *            끝위치
     * @return
     */
    public static String subString(String t, int s, int e) {
        return subString(t, s, e, "");
    }

    /**
     * 지정한 길이만큼만 보여주고 나머지 생략.
     * 
     * @param t
     *            원 문자열
     * @param s
     *            시작위치
     * @param e
     *            끝위치
     * @param omit
     *            생략문자
     * @return
     */
    public static String subString(String t, int s, int e, String omit) {
        if (t.length() > e - s)
            return t.substring(s, e) + omit;
        else
            return t;
    }

    /**
     * 문자열 치환
     * 
     * @param buffer
     *            : 검색문자열
     * @param src
     *            : 치환문자열
     * @param dst
     *            : 변경문자열
     * @return
     */
    public static String replace(String buffer, String src, String dst) {
        if (buffer == null)
            return null;
        if (buffer.indexOf(src) < 0)
            return buffer;

        int bufLen = buffer.length();
        int srcLen = src.length();
        StringBuffer result = new StringBuffer();

        int i = 0;
        int j = 0;
        for (; i < bufLen;) {
            j = buffer.indexOf(src, j);
            if (j >= 0) {
                result.append(buffer.substring(i, j));
                result.append(dst);

                j += srcLen;
                i = j;
            } else
                break;
        }
        result.append(buffer.substring(i));
        return result.toString();
    }

    public static int getByte(String str) {
        int en = 0;
        int ko = 0;
        int etc = 0;

        char[] string = str.toCharArray();

        for (int j = 0; j < string.length; j++) {
            if (string[j] >= 'A' && string[j] <= 'z') {
                en++;
            } else if (string[j] >= '\uAC00' && string[j] <= '\uD7A3') {
                ko++;
                ko++;
            } else {
                etc++;
            }
        }

        return (en + ko + etc);
    }

    public static String unscript(String data) {
        if (data == null || data.trim().equals("")) {
            return "";
        }

        String ret = data;

        ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
        ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");

        ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
        ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");

        ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
        ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");

        ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
        ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");

        ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
        ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");

        return ret;
    }

    public static Object null2EmptyString(Object str) {
        if (str == null)
            str = "";

        return str + "";
    }

    public static String null2EmptyString(String str) {
        if (str == null)
            str = "";

        return str;
    }

    /** 배너 HTML */
    public static String getBanner(String bannerType, String width, String height, String fileExtsn, String fileDownUrl, String linkUrl, String bannerTarget) throws Exception {
        String banner = "";

        if (bannerType.equals("img")) {
            if (fileExtsn.equals("swf")) {
                banner = "<script type=\"text/javascript\">flashWrite('" + fileDownUrl + "','" + width + "','" + height + "','','','', 'transparent');</script>";
            } else {
                if (!StringUtil.isEmpty(width) && !StringUtil.isEmpty(height)) {
                    banner = "<img src=\"" + fileDownUrl + "\" width=\"" + width + "\" height=\"" + height + "\"/>";
                } else if (!StringUtil.isEmpty(width) && StringUtil.isEmpty(height)) {
                    banner = "<img src=\"" + fileDownUrl + "\" width=\"" + width + "\"/>";
                } else if (StringUtil.isEmpty(width) && !StringUtil.isEmpty(height)) {
                    banner = "<img src=\"" + fileDownUrl + "\" height=\"" + height + "\"/>";
                } else {
                    banner = "<img src=\"" + fileDownUrl + "\"/>";
                }

                if (!StringUtil.isEmpty(linkUrl)) {
                    if (bannerTarget.equals("Y")) {
                        banner = "<a href=\"" + linkUrl + "\" target=\"_blank\">" + banner + "</a>";
                    } else {
                        banner = "<a href=\"" + linkUrl + "\">" + banner + "</a>";
                    }
                }
            }
        } else if (bannerType.equals("bg")) {
            if (fileExtsn.equals("swf")) {
                banner = fileDownUrl;
            } else {
                banner = fileDownUrl;
            }
        }

        return banner;
    }

    private final String movServerIp = "211.233.4.120"; // 스트림 서버 IP

    private final String movKeyDir = "/home/pagodasm/WORKAREA/pagodaglobal/WEB-INF/key"; // 함수키
                                                                                         // 디렉토리

    private final String mmsURL = "mms://npagoda.hvod.nefficient.co.kr/npagoda"; // 스트림
                                                                                 // 기본
                                                                                 // 경로

    // 랜덤코드
    public static String randomStr(int len) {
        StringBuffer sb = new StringBuffer();
        String[] apaApChr = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
        sb.setLength(0);
        for (int j = 0; j < len; j++) {
            int vj = (int) (Math.random() * apaApChr.length);
            sb.append(apaApChr[vj]);
        }// for
        return sb.toString();

    }

    // MMS 정보 관련 스트림 인증 프로세서
    @SuppressWarnings("static-access")
	public String mmsReturnKey(String dirfile) {
        StringBuffer sb = new StringBuffer();
        String ramchar = this.randomStr(7);

        // ./MediaAuth2KeyGen /perium/perium_wr_01.wmv king123 211.233.87.36
        sb.setLength(0);
        sb.append(this.movKeyDir + "/MediaAuth2KeyGen " + dirfile + " " + ramchar + " " + this.movServerIp);
        String cmd = sb.toString();

        sb.setLength(0);
        try {
            java.lang.Process p = java.lang.Runtime.getRuntime().exec(cmd);
            java.io.InputStream in = p.getInputStream();
            int i;
            while ((i = in.read()) != -1) {
                sb.append((char) i);
            }

        } catch (java.io.IOException e) {
            sb.setLength(0);
            sb.append("ERROR");
        }

        return sb.toString();
        // return cmd;
    }// function

    public String getMmsURL() {
        return mmsURL;
    }

    private static interface Patterns {
        // javascript tags and everything in between
        public static final Pattern SCRIPTS = Pattern.compile("<(no)?script[^>]*>.*?</(no)?script>", Pattern.DOTALL);

        public static final Pattern STYLE = Pattern.compile("<style[^>]*>.*</style>", Pattern.DOTALL);

        // HTML/XML tags
        public static final Pattern TAGS = Pattern.compile("<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>");

        @SuppressWarnings("unused")
		public static final Pattern nTAGS = Pattern.compile("<\\w+\\s+[^<]*\\s*>");

        // entity references
        public static final Pattern ENTITY_REFS = Pattern.compile("&[^;]+;");

        // repeated whitespace
        public static final Pattern WHITESPACE = Pattern.compile("\\s\\s+");
    }

    public static String cleanTags(String s) {
        if (s == null) {
            return null;
        }

        Matcher m;

        m = Patterns.SCRIPTS.matcher(s);
        s = m.replaceAll("");
        m = Patterns.STYLE.matcher(s);
        s = m.replaceAll("");
        m = Patterns.TAGS.matcher(s);
        s = m.replaceAll("");
        m = Patterns.ENTITY_REFS.matcher(s);
        s = m.replaceAll("");
        m = Patterns.WHITESPACE.matcher(s);
        s = m.replaceAll(" ");

        return s;
    }
}